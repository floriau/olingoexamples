package com.xft.olingousageexamples;

	import java.io.IOException;
	import java.io.InputStream;
	import java.io.UnsupportedEncodingException;
	import java.net.HttpURLConnection;
	import java.net.MalformedURLException;
	import java.net.ProtocolException;
	import java.net.URL;
	import java.net.URLEncoder;

	import org.apache.olingo.odata2.api.edm.Edm;
	import org.apache.olingo.odata2.api.edm.EdmException;
	import org.apache.olingo.odata2.api.ep.EntityProvider;
	import org.apache.olingo.odata2.api.ep.EntityProviderException;
	import org.apache.olingo.odata2.api.ep.EntityProviderReadProperties;
	import org.apache.olingo.odata2.api.ep.entry.ODataEntry;
	import org.apache.olingo.odata2.api.ep.feed.ODataFeed;
	import org.apache.olingo.odata2.api.exception.ODataException;
    import org.apache.olingo.sample.client.OlingoSampleApp;

	/****************************************************************************** 
	 *                                                                            *
	 * Das ist keine Klasse, sondern ein Haufen skriptartiger Beispielquelltexte  *
	 *                                                                            *
	 ******************************************************************************/
	public class ExampleClientUsage {
	    
		private static final String serviceUrl = "http://services.odata.org/V2/Northwind/Northwind.svc"; //"http://localhost:8080/MyFormula.svc";
		
		public static void main(String[] args) {
			System.out.println( "==== Employee with EmployeeID 4 ===============" );
			readEmployeeOne();
			System.out.println( "==== All Employees ============================" );
			readAllEmployees();
			System.out.println( "==== Employees with 'll' in their Last Name ===" );
			queryEmployees();
		}
		
		private static int readEmployeeOne() {
			OlingoSampleApp helper = new OlingoSampleApp();
			/* folgende trys können zu einem verkürzt werden: } catch (ODataException | IOException  e) {  */
			try {
				Edm edm = helper.readEdm( serviceUrl );
				try {
					ODataEntry entry = helper.readEntry( edm, serviceUrl, 
							                             OlingoSampleApp.APPLICATION_ATOM_XML, "Employees", "4" ); //"Manufacturers", "'1'" ); 
					String employeeOnesLastName  = (String) entry.getProperties().get( "LastName"  );//
					String employeeOnesFirstName = (String) entry.getProperties().get( "FirstName" );//
					System.out.println( employeeOnesFirstName + " " + employeeOnesLastName );        // System.out.println( entry.toString() );  
					return 0;
				} catch (ODataException e) {
					System.out.println( "Error optaining the Default Entity Container from the buffered EDM" );
				} catch (IOException e) {
					System.out.println( "Error fetching Entry you requested from " + serviceUrl );
				}
			} catch (ODataException e) {
				System.out.println( "Error parsing the buffered metadata from " + serviceUrl );
			} catch (IOException e) {
				System.out.println( "Error fetching the metadata from " + serviceUrl );
			}
			return 1;
		}

		private static int readAllEmployees() {
			OlingoSampleApp utils = new OlingoSampleApp();
			/* folgende trys können zu einem verk�rzt werden: } catch (ODataException | IOException  e) {  */
			try {
				Edm edm = utils.readEdm( serviceUrl );
				try {
					ODataFeed employees = utils.readFeed( edm, serviceUrl, OlingoSampleApp.APPLICATION_ATOM_XML, "Employees" );
					String employeesLastName;
					String employeesFirstName;
					String employeesEmployeeID;
					for (ODataEntry employee : employees.getEntries()) {
						employeesLastName   = (String) employee.getProperties().get( "LastName"   );
						employeesFirstName  = (String) employee.getProperties().get( "FirstName"  );
						employeesEmployeeID =          employee.getProperties().get( "EmployeeID" ).toString();
				        System.out.println( employeesFirstName + " " + employeesLastName + " (ID:" + employeesEmployeeID +")" );
				    }  
					return 0;
				} catch (ODataException e) {
					System.out.println( "Error optaining the Default Entity Container from the buffered EDM" );
				} catch (IOException e) {
					System.out.println( "Error fetching Entry you requested from " + serviceUrl );
				}
			} catch (ODataException e) {
				System.out.println( "Error parsing the buffered metadata from " + serviceUrl );
			} catch (IOException e) {
				System.out.println( "Error fetching the metadata from " + serviceUrl );
			}
			return 1;
		}
		
		private static int queryEmployees() { /* Dieses Beispiel kommt komplett ohne den OlingoHelper aus! */
			Edm edm = null;
			try {
			    URL metatdataURL = new URL( serviceUrl + "/$metadata" );
			    HttpURLConnection metatdataConnection = (HttpURLConnection) metatdataURL.openConnection();
			    metatdataConnection.setRequestMethod( "GET" );
			    metatdataConnection.setRequestProperty( "Accept", "application/xml" );
			    metatdataConnection.setUseCaches( false ); /* zum Testen besser geeignet */
			    InputStream metadata = metatdataConnection.getInputStream(); /* hier schlägt Olingo einen HTTP-Return-Code-Check vor */
			    edm = EntityProvider.readMetadata( metadata, false );
			} catch (MalformedURLException e) {
				System.out.println( "Constructing the URL to the metadata of OData-Service failed" );
			} catch (ProtocolException | SecurityException e) {
				System.out.println( "The connection for metadata retreival doesn't allow the HTTP method GET." );
			} catch (IOException e) {
				System.out.println( "opening the HTTP connection to the OData-Service's metadata failed, or getting the corresponding input stream" );
			} catch (IllegalStateException | NullPointerException e) {
				System.out.println( "setting the metadata retreival connection's request property or disabling caching failed" );
			} catch (EntityProviderException e) {
				System.out.println( "Deserializing the received XML metadata by Olingo failed" );
			}
			if (edm == null) {
				return 1;
			}
	        try {
	        	String queryURL = serviceUrl + URLEncoder.encode("/Employees?$filter=substringof('ll', LastName) eq true", "UTF-8");
	        	URL query = new URL( queryURL ); /*              "/Employees?$filter=substringof(%27ll%27,%20LastName)%20eq%20true" */
	        	HttpURLConnection queryConnection = (HttpURLConnection) query.openConnection();
	        	queryConnection.setRequestMethod( "GET" ); 
	        	queryConnection.setRequestProperty( "Accept", "application/atom+xml" );
	        	queryConnection.setUseCaches( false ); /* WEITERE:.setConnectTimeout( 10000 );  .setAllowUserInteraction( true ); */
	        	InputStream result = queryConnection.getInputStream(); 
				ODataFeed llEmployees = EntityProvider.readFeed( "application/atom+xml",                                      
			    		                                         edm.getDefaultEntityContainer().getEntitySet( "Employees" ),
			    		                                         result, EntityProviderReadProperties.init().build()         );
				String llEmployeesName;
				String llEmployeesFirstName;
				String llEmployeesID;
				for (ODataEntry employee : llEmployees.getEntries()) {
					llEmployeesName      = (String) employee.getProperties().get( "LastName"   );
					llEmployeesFirstName = (String) employee.getProperties().get( "FirstName"  );
					llEmployeesID        =          employee.getProperties().get( "EmployeeID" ).toString();
			        System.out.println( llEmployeesFirstName + " " + llEmployeesName + " (ID:" + llEmployeesID +")" );
			    }  
				return 0;
	        } catch (UnsupportedEncodingException ignored) {
	        	System.out.println( "Encoding the query-URL for an HTTP request failed" );
	        } catch (MalformedURLException e) {
				System.out.println( "Constructing the query-URL for the OData service failed" );
			} catch (ProtocolException | SecurityException e) {
				System.out.println( "The connection for quering doesn't allow the HTTP method GET." );
			} catch (IOException e) {
				System.out.println( "opening the HTTP connection to query the OData service failed, or getting the corresponding input stream" );
			} catch (IllegalStateException | NullPointerException e) {
				System.out.println( "setting the query connection's request property or disabling caching failed" );
			} catch (EntityProviderException | EdmException e) {
				System.out.println( "Deserializing the received XML query result by Olingo failed" );
			}
	        return 1;
		}
		
	}
