README
======

What is this repository for?
----------------------------

This repository hosts Java code to query the publically available Northwind-OData-service with the help of the Olingo library.

How do I get set up and running?
--------------------------------

- Make sure you have the JDK 8 installed on your machine. 
- Download the repository by clicking the cloud with the little arrow on the left and unzip it.
- Run the following command **from inside the unzipped** *OlingoExamples* folder ``gradlew.bat run`` **or** in case your **not on Windows** ``./gradlew run``
